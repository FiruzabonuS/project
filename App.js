// import React from 'react';
// import {
//   StyleSheet,
//   SafeAreaView,
//   View,
//   TouchableOpacity,
//   Text,
// } from 'react-native';
// import {useState} from 'react';
// import Screen from './Screen';
// import Screen2 from './Screen2';

// export default function App() {
//   const [active, setActive] = useState(false);
//   return (
//     <SafeAreaView>
//       <View style={styles.container}>
//         <TouchableOpacity
//           onPress={() => {
//             setActive(true);
//           }}
//           style={[styles.button, {borderColor: active ? 'red' : 'black'}]}>
//           <Text>Screen1</Text>
//         </TouchableOpacity>
//         <TouchableOpacity
//           onPress={() => {
//             setActive(false);
//           }}
//           style={[styles.button, {borderColor: active ? 'black' : 'red'}]}>
//           <Text>Screen2</Text>
//         </TouchableOpacity>
//       </View>
//       {active ? <Screen /> : <Screen2 />}
//     </SafeAreaView>
//   );
// }

// const styles = StyleSheet.create({
//   button: {
//     width: 178,
//     height: 50,
//     backgroundColor: '#C4C4C4',
//     borderRadius: 16,
//     justifyContent: 'center',
//     alignItems: 'center',
//     borderWidth: 2,
//   },
//   container: {
//     flexDirection: 'row',
//     justifyContent: 'space-around',
//   },
// });

import React, {useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import ImagesBlog from './Screen';
import Screen2 from './Screen2';

export default function App() {
  const [active, setActive] = useState(true);
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <TouchableOpacity
          style={[styles.button, {borderColor: active ? 'red' : 'black'}]}
          onPress={() => setActive(true)}>
          <Text>Screen 1</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, {borderColor: active ? 'black' : 'red'}]}
          onPress={() => setActive(false)}>
          <Text>Screen 2</Text>
        </TouchableOpacity>
      </View>
      {active ? <ImagesBlog /> : <Screen2 />}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'yellow',
    width: 150,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
