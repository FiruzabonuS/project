import React, {useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
const data = [
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWmUOvofdKUdxOfLjwXsJiJYHt-yU0XFifxQ&usqp=CAU',
  'https://www.publicdomainpictures.net/pictures/320000/nahled/background-image.png',
  'https://static.addtoany.com/images/dracaena-cinnabari.jpg',
];

export default function ImagesBlog() {
  const [img, setImg] = useState(
    'https://static.addtoany.com/images/dracaena-cinnabari.jpg',
  );
  return (
    <SafeAreaView>
      <View style={styles.bigContainer}>
        <Image style={styles.bigBox} source={{uri: img}} />
      </View>
      <View style={styles.smallContainer}>
        {data.map(image => {
          return (
            <TouchableOpacity onPress={() => setImg(image)}>
              <Image style={styles.smallBox} source={{uri: image}} />
            </TouchableOpacity>
          );
        })}
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  bigContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  bigBox: {
    width: 300,
    height: 300,
    borderRadius: 12,
    marginTop: 40,
  },
  smallBox: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  smallContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 15,
  },
});
