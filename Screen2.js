import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, Image, View} from 'react-native';

export default function Screen2() {
  const [active, setActive] = useState(true);
  return (
    <View style={styles.container}>
      {active ? (
        <Image style={styles.image} source={require('./img/close.png')} />
      ) : (
        <Image style={styles.image} source={require('./img/open.png')} />
      )}
      <TouchableOpacity style={styles.button} onPress={() => setActive(false)}>
        {active ? <Text>Open</Text> : <Text>Close</Text>}
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: 300,
    height: 300,
  },
  button: {
    width: 150,
    height: 50,
    backgroundColor: 'lightblue',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
